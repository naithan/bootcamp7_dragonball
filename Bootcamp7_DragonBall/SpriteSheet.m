//
//  SpriteSheet.m
//  Bootcamp7_DragonBall
//
//  Created by Jeremi Kaczmarczyk on 21.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import "SpriteSheet.h"
#import <SpriteKit/SpriteKit.h>

#define  faceWidth 20
#define  faceHeight 30

#define  hatWidth 30
#define  hatHeight 40

#define  hairWidth 30
#define  hairHeight 50

#define chickenWidth 255
#define chickenHeight 256

@interface SpriteSheet()
@property(strong,nonatomic) SKTexture *spriteSheet;
@end

@implementation SpriteSheet
-(instancetype)initWithSpriteSheetName: (NSString *)name {
    self = [super init];
    if (self) {
        self.spriteSheet = [SKTexture textureWithImageNamed:name];
    }return  self;
}

+(CGRect)translateCoordinates: (CGRect)rect fromSize: (CGSize)size {
    CGRect unitRect = CGRectMake(rect.origin.x/size.width,
                                 1-(rect.size.height + rect.origin.y)/size.height,
                                 rect.size.width/size.width,
                                 rect.size.height/size.height);
    
    return unitRect;
}

- (SKTexture *)getFace:(NSInteger)index {
    
    return [self getSubTexture:CGRectMake((index*faceWidth)+210, 10, faceWidth, faceHeight)];
}

- (SKTexture *)getHair:(NSInteger)index {
    
    return [self getSubTexture:CGRectMake((index*hairWidth)+210, 40, hairWidth , hairHeight)];
}

- (SKTexture *)getHat:(NSInteger)index {
    
    return [self getSubTexture:CGRectMake((index*hatWidth)+200, 90, hatWidth , hatHeight)];
}

-(SKTexture *)getSubTexture: (CGRect)rect {
    return [SKTexture textureWithRect:[SpriteSheet translateCoordinates: rect fromSize:self.spriteSheet.size] inTexture:self.spriteSheet];
    
}

-(SKTexture *)getChicken:(NSInteger)index {

    //return [self getSubTexture:CGRectMake(0 + (index*254), 22, 256, 256)];
    if (index < 4) {
        return [self getSubTexture:CGRectMake((index*254)+0, 0, 254, 258)];
    }else if (index < 8) {
        return [self getSubTexture:CGRectMake(((index-4)*254)+0, 0+258, 254, 258)];
        
    }else if (index < 12) {
        return [self getSubTexture:CGRectMake(((index-8)*254)+0, 0+258*2, 254, 258)];
        
    }else {
        return [self getSubTexture:CGRectMake(((index-12)*254)+0, 0+258*3, 254, 258)];
    }
}
@end
