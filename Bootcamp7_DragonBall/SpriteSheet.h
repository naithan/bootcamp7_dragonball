//
//  SpriteSheet.h
//  Bootcamp7_DragonBall
//
//  Created by Jeremi Kaczmarczyk on 21.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>


@class NSTexture;
@interface SpriteSheet : NSObject

-(instancetype)initWithSpriteSheetName: (NSString *)name;
- (SKTexture *)getFace:(NSInteger)index;
- (SKTexture *)getHair:(NSInteger)index;
- (SKTexture *)getHat:(NSInteger)index ;
- (SKTexture *)getChicken:(NSInteger)index ;

+(CGRect)translateCoordinates: (CGRect)rect fromSize: (CGSize)size ;





@end
