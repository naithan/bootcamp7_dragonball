//
//  AppDelegate.h
//  Bootcamp7_DragonBall
//
//  Created by Jeremi Kaczmarczyk on 21.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

