//
//  GameScene.m
//  Bootcamp7_DragonBall
//
//  Created by Jeremi Kaczmarczyk on 21.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import "GameScene.h"
#import "SpriteSheet.h"
@interface GameScene()

@property SpriteSheet *worker;
@property SKSpriteNode *chickenNode;
@property NSInteger counter;

@end

@implementation GameScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    self.counter = 1;
    self.worker = [[SpriteSheet alloc] initWithSpriteSheetName:@"birdSpriteSheet.png"];
    
    self.chickenNode = [[SKSpriteNode alloc] initWithTexture:[self.worker getChicken:_counter]];
    self.chickenNode.position = self.view.center;
    [self addChild:self.chickenNode];
    
    
//    self.hairCounter = 1;
//    SKTexture *core = [SKTexture textureWithImageNamed:@"spriteSheet"];
//    
//    CGRect bodyRect = CGRectMake(0, 1 - 90/core.size.height, 80/core.size.width, 90/core.size.height);
//    
//    SKTexture *body = [SKTexture textureWithRect:bodyRect inTexture:core];
//    SKSpriteNode *bodyNode = [[SKSpriteNode alloc] initWithTexture:body];
//    bodyNode.position = self.view.center;
//    [self addChild:bodyNode];
//    
//    SpriteSheet *getFace = [[SpriteSheet alloc] initWithSpriteSheetName:@"spriteSheet"];
//    SKTexture *face = [getFace getFace:1];
//    SKSpriteNode *faceNode = [[SKSpriteNode alloc] initWithTexture:face];
//    faceNode.position = CGPointMake(3, 7);
//    [bodyNode addChild:faceNode];
//    
//    SpriteSheet *getHair = [[SpriteSheet alloc] initWithSpriteSheetName:@"spriteSheet"];
//    SKTexture *hair = [getHair getHair:self.hairCounter];
//    SKSpriteNode *hairNode = [[SKSpriteNode alloc] initWithTexture:hair];
//    hairNode.position = CGPointMake(-2, 10);
//    [bodyNode addChild:hairNode];
//    
//    SpriteSheet *getHat = [[SpriteSheet alloc] initWithSpriteSheetName:@"spriteSheet"];
//    SKTexture *hat = [getHat getHat:0];
//    SKSpriteNode *hatNode = [[SKSpriteNode alloc] initWithTexture:hat];
//    hatNode.position = CGPointMake(1, 11);
//    [bodyNode addChild:hatNode];
    
    //hairNode.position = [hairNode convertPoint:bodyNode.position fromNode:bodyNode];
    
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSInteger i = 0; i<16; i++){
        [array addObject:[self.worker getChicken:i]];
    }
    
    [self.chickenNode runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:array timePerFrame:.2]]];
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}



@end
